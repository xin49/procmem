import os
import datetime
import shutil
import sys
import signal
import time

def Split_String(argStrSrc):
    return argStrSrc.split(" ", len(argStrSrc))

def StringListStrip(argStrLst, argStrDel):
    cnt = len(argStrLst)
    idx = 0
    strLstRst = []
    while(idx < cnt):
        if(argStrLst[idx] != argStrDel):
            strLstRst.append(argStrLst[idx])
        idx = idx + 1
    return strLstRst

def Get_ProcessMem_Cmd(argProcId):
    cmd1 = "cat /proc/"
    cmd2 = "/status | grep VmRSS"
    return cmd1 + argProcId + cmd2

def StrInLst(argStr, argLst):
    for str in argLst:
        if(argStr == str):
            return True
    return False
    
def get_processid(argAppName):
    cmd = "ps -aux | grep " + argAppName
    rstLst = os.popen(cmd).readlines()
    for line in rstLst:
        strLst = line.split()
        if(not StrInLst("./procmem.py", strLst)):
            if(not StrInLst("vim", strLst)):
                print(strLst)
                return strLst[1]
    return -1

if __name__ == '__main__':
    argLst = sys.argv
    print(argLst)
    processId = -1
    while(processId == -1):
        processId = get_processid(argLst[1])
        time.sleep(1)
    print(processId)
    cmd = Get_ProcessMem_Cmd(processId)
    count = 0
    procMem = 0
    maxProcMem = 0
    while True:
        try:
            strNow = os.popen(cmd).readlines()[0]
            strLst = Split_String(strNow)
            strLst = StringListStrip(strLst, '')
            procMem = int(strLst[1])
            if(procMem > maxProcMem):
                maxProcMem = procMem
                print("MaxMem:", str(maxProcMem)+" kb")
            count = count + 1
        except KeyboardInterrupt:
            input("")
            #sys.exit()
